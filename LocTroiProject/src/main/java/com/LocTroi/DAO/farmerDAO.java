package com.LocTroi.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.Model.farmerEntity;
import com.LocTroi.Repository.farmerRepository;

@Component
public class farmerDAO {
	
	@Autowired
	private farmerRepository farmerrepository;
	
	public farmerEntity getByFarmerId (int farmerId) {
		
		return farmerrepository.findById(farmerId);
		
	}
	
	

}
