package com.LocTroi.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.Model.districtsEntity;
import com.LocTroi.Repository.districtsRepository;

@Component
public class DitrictDAO {
	@Autowired
	private districtsRepository districtReposi;
	
	public districtsEntity districEntity(int districtsId) {
		return districtReposi.findById(districtsId);
	}
	
}
