package com.LocTroi.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.Model.userEntity;
import com.LocTroi.Repository.userRepository;

@Component
public class UserDAO {
	
	@Autowired
	private userRepository userReposi ;
	
	public userEntity getByUserId(int userId) {
		return userReposi.findById(userId);
	}
	
	
	

}
