package com.LocTroi.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.Model.citiesEntity;
import com.LocTroi.Repository.cityRepository;

@Component
public class CitiDAO {
	@Autowired 
	private cityRepository cityRePoSi;
	
	public citiesEntity getByCitiId (int citiId) {
		return cityRePoSi.findByCity(citiId);
	}

}
