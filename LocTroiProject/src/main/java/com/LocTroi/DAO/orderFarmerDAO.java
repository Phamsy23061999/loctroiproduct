package com.LocTroi.DAO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.DTO.citiesDTO;
import com.LocTroi.DTO.districtsDTO;
import com.LocTroi.DTO.farmerDTO;
import com.LocTroi.DTO.orderFarmerDTO;
import com.LocTroi.DTO.userDTO;
import com.LocTroi.DTO.wardsDTO;
import com.LocTroi.Model.citiesEntity;
import com.LocTroi.Model.districtsEntity;
import com.LocTroi.Model.farmerEntity;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Model.userEntity;
import com.LocTroi.Model.wardsEntity;
import com.LocTroi.Reponese.FarmerReponse;
import com.LocTroi.Reponese.OrderFarmerReponese;
import com.LocTroi.Repository.cityRepository;
import com.LocTroi.Repository.districtsRepository;
import com.LocTroi.Repository.farmerRepository;
import com.LocTroi.Repository.orderFarmerRepository;
import com.LocTroi.Repository.userRepository;
import com.LocTroi.Repository.wardsRepository;

import net.minidev.json.JSONObject;

@Component
public class orderFarmerDAO {
	
	@Autowired 
	private orderFarmerRepository repository;

	@Autowired
	private farmerDAO farmerdao;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private CitiDAO citiDAO;
	@Autowired
	private DitrictDAO ditrictDAO;
	@Autowired
	private wardDAO wardsDAO;
	
	
	public List<OrderFarmerReponese> getByOrderFarmer(Date dateStart, Date dateFinish) {
		
		List<orderFarmerEntity> listOrder = repository.findByFarmerOrderCreatedate(dateStart,dateFinish);
		List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
		
		for(orderFarmerEntity order : listOrder){
				
			farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
		
			userEntity user = userDAO.getByUserId(order.getUserId());

			citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

			districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
			
			wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

				
			OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
			
			
			listReponse.add(repo);
		}
		return listReponse;
				
		
}
	
	public List<OrderFarmerReponese> finByOrderAgency(String agencyName){
		
		List<orderFarmerEntity> listOrder = repository.finByOrderAgancy(agencyName);
		List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
		for(orderFarmerEntity order : listOrder){
			
			farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
		
			userEntity user = userDAO.getByUserId(order.getUserId());

			citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

			districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
			
			wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

				
			OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
			
			
			listReponse.add(repo);
		}
		return listReponse;
				
		
		
	}
	
	public List<OrderFarmerReponese> finByOrderDistrict(String districtsName){
		List<orderFarmerEntity> listOrder = repository.finByOrderDistrict(districtsName);
		List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
		
		
		for(orderFarmerEntity order : listOrder){
			
			farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
		
			userEntity user = userDAO.getByUserId(order.getUserId());

			citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

			districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
			
			wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

				
			OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
			
			
			listReponse.add(repo);
		}
		return listReponse;
							
					
					
		}
	public List<OrderFarmerReponese> finByOrderWard(String wardName){
		List<orderFarmerEntity> listOrder = repository.finByOrderWard(wardName);
		List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
		for(orderFarmerEntity order : listOrder){
			
			farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
		
			userEntity user = userDAO.getByUserId(order.getUserId());

			citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

			districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
			
			wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

				
			OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
			
			
			listReponse.add(repo);
		}
		return listReponse;
		
		
	}
	
	public List<OrderFarmerReponese> finByOrderCiti(String citiName){
		List<orderFarmerEntity> listOrder = repository.finByOrderCiti(citiName);
		List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
		for(orderFarmerEntity order : listOrder){
			
			farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
		
			userEntity user = userDAO.getByUserId(order.getUserId());

			citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

			districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
			
			wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

				
			OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
			
			
			listReponse.add(repo);
		}
		return listReponse;
				
			
	}
		

		public List<OrderFarmerReponese> findByOrderAll(Date dateStart, Date dateFinish, int cityId, int wardId, int districtId,int agencyId){
			List<orderFarmerEntity> listOrder = repository.findByOrderAll(dateStart, dateFinish, cityId, wardId, districtId, agencyId);
			List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
			
			for(orderFarmerEntity order : listOrder){
				
				farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
			
				userEntity user = userDAO.getByUserId(order.getUserId());

				citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

				districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
				
				wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

					
				OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
				
				
				listReponse.add(repo);
			}
			return listReponse;
				
	}
		
		
		
		public  JSONObject getByOrderKeyName (String keysearch, int page, int size){
			
			int begin = (page - 1)* size ;
			
			
			List<orderFarmerEntity> listOrder = repository.getByOrderKeyName(keysearch,begin,size);
			int total = repository.totalOrder(keysearch);
			
			JSONObject js = new JSONObject();
			List<OrderFarmerReponese> listReponse = new ArrayList<OrderFarmerReponese>();
			
			for(orderFarmerEntity order : listOrder){
				
				farmerEntity farmer = farmerdao.getByFarmerId(order.getFarmerId());
			
				userEntity user = userDAO.getByUserId(order.getUserId());

				citiesEntity citi = citiDAO.getByCitiId(order.getCityId());

				districtsEntity districts = ditrictDAO.districEntity(order.getDistrictId());
				
				wardsEntity wards = wardsDAO.getByWardId(order.getWardId());

					
				OrderFarmerReponese repo = new OrderFarmerReponese(order, farmer, user, citi, districts, wards);
				
				listReponse.add(repo);		
				
				
			}
			js.put("List Order", listReponse);
			js.put("total", total);
			js.put("Size", size);
			return js;
			
	}
			
}
		

			
			

	
	
	
		
		
	
		
	
	

