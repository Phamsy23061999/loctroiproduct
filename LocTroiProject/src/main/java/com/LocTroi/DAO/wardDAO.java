package com.LocTroi.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.LocTroi.Model.wardsEntity;
import com.LocTroi.Repository.wardsRepository;

@Component
public class wardDAO {
	
	@Autowired
	private wardsRepository wardReposi;
	
	public wardsEntity getByWardId(int wardId) {
		return wardReposi.findById(wardId);
	}

}
