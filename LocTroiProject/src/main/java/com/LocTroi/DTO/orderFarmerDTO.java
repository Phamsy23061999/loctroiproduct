package com.LocTroi.DTO;

import java.security.Timestamp;
import java.util.Date;

import org.modelmapper.ModelMapper;

public class orderFarmerDTO {
	
	private int id;
	private String orderCode;
	private int farmerId;
	private String phone;
	private String fullName;
	
	private int wardId;
	private int districId;
	private int cityId;
	private int status;
	private int deleted;
	private Double totalMoneyPay;
	private Double totalMoneyBook;
	private Double totalMoneyDiscount;
	private int order_type;
	private int orderManId;
	private Date createDate;
	private Timestamp updateTime;
	private String note;
	
	
	

	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public int getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public int getWardId() {
		return wardId;
	}
	public void setWardId(int wardId) {
		this.wardId = wardId;
	}
	public int getDistricId() {
		return districId;
	}
	public void setDistricId(int districId) {
		this.districId = districId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getDeleted() {
		return deleted;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	public Double getTotalMoneyPay() {
		return totalMoneyPay;
	}
	public void setTotalMoneyPay(Double totalMoneyPay) {
		this.totalMoneyPay = totalMoneyPay;
	}
	public Double getTotalMoneyBook() {
		return totalMoneyBook;
	}
	public void setTotalMoneyBook(Double totalMoneyBook) {
		this.totalMoneyBook = totalMoneyBook;
	}
	public Double getTotalMoneyDiscount() {
		return totalMoneyDiscount;
	}
	public void setTotalMoneyDiscount(Double totalMoneyDiscount) {
		this.totalMoneyDiscount = totalMoneyDiscount;
	}
	public int getOrder_type() {
		return order_type;
	}
	public void setOrder_type(int order_type) {
		this.order_type = order_type;
	}
	public int getOrderManId() {
		return orderManId;
	}
	public void setOrderManId(int orderManId) {
		this.orderManId = orderManId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public static <T> T transferObject(Object model, Class<T> type){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(model, type);
    }

}
