package com.LocTroi.Model;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.aspectj.weaver.tools.Trace;

@Entity
public class userEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "sale_man_code")
	private String saleManCode;
	
	@Column(name = "address")
	private String address;
	
//	@Column(name = "avartar")
//	private String avartar;
	
	@Column(name = "cities_id")
	private int citiesId;
	
	@Column(name = "country_code")
	private String countryCode;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "deviceid")
	private String deviceId;
	
	@Column(name = "district_id")
	private int districtId;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "firebase_devicetoken")
	private String firebaseDevicetoken;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "os")
	private String os;
	
	@Column(name = "otp")
	private String otp;
	
	@Column(name = "shop_name")
	private String shopName;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "update_time")
	private Time updateTime;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "ward_id")
	private int wardId;
	
	@Column(name = "vip")
	private int vip;
	
	@Column(name = "level_up_require")
	private String levelUpRequire;
	
	@Column(name = "total_buy")
	private int total_buy;
	
	@Column(name = "point")
	private int point;
	
	@Column(name = "supplier_id")
	private int supplierId;
	
	@Column(name = "view")
	private int view;
	
	@Column(name = "shop_type")
	private int shopType;
	
	@Column(name = "shop_channel")
	private int shopChannel;
	
	@Column(name = "active_push")
	private int activePush;
	
//	@Column(name = "phone2")
//	private String phone2;
	
	@Column(name = "bussiness_name")
	private String bussinessName;
	
	@Column(name = "bussiness_tax_code")
	private String bussinessTaxCode;
	
	@Column(name = "bussiness_address")
	private String bussinessAddress;
	
	@Column(name = "images")
	private String images;
	
	@Column(name = "lat")
	private Double lat;
	
	@Column(name = "long")
	private Double Long;
	
	@Column(name = "dms_code")
	private String dmsCode;
	
	@Column(name = "address_delivery")
	private String address_delivery;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSaleManCode() {
		return saleManCode;
	}

	public void setSaleManCode(String saleManCode) {
		this.saleManCode = saleManCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

//	public String getAvartar() {
//		return avartar;
//	}
//
//	public void setAvartar(String avartar) {
//		this.avartar = avartar;
//	}

	public int getCitiesId() {
		return citiesId;
	}

	public void setCitiesId(int citiesId) {
		this.citiesId = citiesId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirebaseDevicetoken() {
		return firebaseDevicetoken;
	}

	public void setFirebaseDevicetoken(String firebaseDevicetoken) {
		this.firebaseDevicetoken = firebaseDevicetoken;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Time getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Time updateTime) {
		this.updateTime = updateTime;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getWardId() {
		return wardId;
	}

	public void setWardId(int wardId) {
		this.wardId = wardId;
	}

	public int getVip() {
		return vip;
	}

	public void setVip(int vip) {
		this.vip = vip;
	}

	public String getLevelUpRequire() {
		return levelUpRequire;
	}

	public void setLevelUpRequire(String levelUpRequire) {
		this.levelUpRequire = levelUpRequire;
	}

	public int getTotal_buy() {
		return total_buy;
	}

	public void setTotal_buy(int total_buy) {
		this.total_buy = total_buy;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public int getView() {
		return view;
	}

	public void setView(int view) {
		this.view = view;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getShopChannel() {
		return shopChannel;
	}

	public void setShopChannel(int shopChannel) {
		this.shopChannel = shopChannel;
	}

	public int getActivePush() {
		return activePush;
	}

	public void setActivePush(int activePush) {
		this.activePush = activePush;
	}

//	public String getPhone2() {
//		return phone2;
//	}
//
//	public void setPhone2(String phone2) {
//		this.phone2 = phone2;
//	}

	public String getBussinessName() {
		return bussinessName;
	}

	public void setBussinessName(String bussinessName) {
		this.bussinessName = bussinessName;
	}

	public String getBussinessTaxCode() {
		return bussinessTaxCode;
	}

	public void setBussinessTaxCode(String bussinessTaxCode) {
		this.bussinessTaxCode = bussinessTaxCode;
	}

	public String getBussinessAddress() {
		return bussinessAddress;
	}

	public void setBussinessAddress(String bussinessAddress) {
		this.bussinessAddress = bussinessAddress;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLong() {
		return Long;
	}

	public void setLong(Double l) {
		Long = l;
	}

	public String getDmsCode() {
		return dmsCode;
	}

	public void setDmsCode(String dmsCode) {
		this.dmsCode = dmsCode;
	}

	public String getAddress_delivery() {
		return address_delivery;
	}

	public void setAddress_delivery(String address_delivery) {
		this.address_delivery = address_delivery;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
