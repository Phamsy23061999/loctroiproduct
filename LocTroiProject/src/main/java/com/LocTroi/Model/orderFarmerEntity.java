package com.LocTroi.Model;

import java.math.BigInteger;
import java.security.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

public class orderFarmerEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="order_code")
	private String orderCode;

	
	@Column(name = "status")
	private int status;
	
	@Column(name ="create_date")
	private Date createDate;
	
	@Column(name = "farmer_id")
	private int farmerId;
	
	@Column(name = "ward_id")
	private int wardId;
	
	@Column(name ="district_id")
	private int districtId;
	
	@Column(name = "city_id")
	private int cityId;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name = "total_money_finish")
	private BigInteger totalMoneyFinish;
	

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public int getWardId() {
		return wardId;
	}

	public void setWardId(int wardId) {
		this.wardId = wardId;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	

	public BigInteger getTotalMoneyFinish() {
		return totalMoneyFinish;
	}

	public void setTotalMoneyFinish(BigInteger totalMoneyFinish) {
		this.totalMoneyFinish = totalMoneyFinish;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public orderFarmerEntity(int id, String orderCode, int status, Date createDate, int farmerId, int wardId,
			int districtId, int cityId, int userId, BigInteger totalMoneyFinish) {
		super();
		this.id = id;
		this.orderCode = orderCode;
		this.status = status;
		this.createDate = createDate;
		this.farmerId = farmerId;
		this.wardId = wardId;
		this.districtId = districtId;
		this.cityId = cityId;
		this.userId = userId;
		this.totalMoneyFinish = totalMoneyFinish;
	}

	public orderFarmerEntity() {
		super();
	}
	


}
