package com.LocTroi.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OrderChildProductEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderCodeChild() {
		return orderCodeChild;
	}

	public void setOrderCodeChild(String orderCodeChild) {
		this.orderCodeChild = orderCodeChild;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name="order_code_child")
	private String orderCodeChild;
	
	@Column(name = "shop_name")
	private String shopName;
}
