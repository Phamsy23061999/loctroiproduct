package com.LocTroi.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProductEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="product_code")
	private String productCode;
	
	@Column(name = "name")
	private String name;
	
	@Column(name= "price")
	private Double price;
	
	@Column (name = "min_unit")
	private int minUnit;
	
	@Column (name = " max_unit")
	private int maxUnit;
	
	@Column(name="attribute")
	private String attribute;
	
	@Column(name = "image_thumbnail")
	private String imageThumbnail;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getMinUnit() {
		return minUnit;
	}

	public void setMinUnit(int minUnit) {
		this.minUnit = minUnit;
	}

	public int getMaxUnit() {
		return maxUnit;
	}

	public void setMaxUnit(int maxUnit) {
		this.maxUnit = maxUnit;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getImageThumbnail() {
		return imageThumbnail;
	}

	public void setImageThumbnail(String imageThumbnail) {
		this.imageThumbnail = imageThumbnail;
	}
	
	

}
