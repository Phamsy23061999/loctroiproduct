package com.LocTroi.Rest;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.LocTroi.ServiceImpl.orderFarmerServiceIMPL;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/order")
public class orderFarmerRestController {
	
	@Autowired
	orderFarmerServiceIMPL orderIMPL;
	
	@GetMapping("/get/order_farmer")
	public JSONObject findByOrderFarmer(@RequestParam("dateStart")Date dateStart,@RequestParam("dateFinish") Date dateFinish) {
		return orderIMPL.findByOrderFarmer(dateStart, dateFinish);
	}
	@GetMapping("/get/order_agency_name")
	public JSONObject findByOrderAgency(@RequestParam("agencyName") String agencyName ) {
		return orderIMPL.findByOrderAgency(agencyName);
	}
	
	@GetMapping("/get/order_districts_name")
	public JSONObject findByOrderDistricts(@RequestParam("districtsName") String districtsName ) {
		return orderIMPL.findByOrderDistricts(districtsName);
	}
	
	@GetMapping("/get/order_ward_name")
	public JSONObject findByOrderWard(@RequestParam("wardName") String wardName ) {
		return orderIMPL.findByOrderWard(wardName);
		
	}
	
	@GetMapping("/get/order_citi_name")
	public JSONObject findByOrderCiti(@RequestParam("citiName") String citiName ) {
		return orderIMPL.findByOrderCiti(citiName);
		
	}
	
	@GetMapping("/get/order_All")
	public JSONObject findByOrderAll(@RequestParam("dateStart")Date dateStart,@RequestParam("dateFinish") Date dateFinish,@RequestParam("cityId") 
	int cityId, @RequestParam("wardId")int wardId, @RequestParam("districtId") int districtId ,@RequestParam("agencyId") int agencyId ) {
		return orderIMPL.finByOrderAll(dateStart, dateFinish, cityId, wardId, districtId, agencyId);
	}
	
	@GetMapping("/get/order_Keyseach")
	public JSONObject getByOrderKyname (@RequestParam("keysearch") String keysearch, @RequestParam("page") int page, @RequestParam("size") int size) {
		return orderIMPL.findByOrder(keysearch, page, size);
	}
	
	
	

}
 