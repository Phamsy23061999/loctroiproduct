package com.LocTroi.Rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.LocTroi.Service.ProductService;
import com.LocTroi.ServiceImpl.ProductServiceIMPL;

import net.minidev.json.JSONObject;

@Controller
public class ProductRestController {
	@Autowired
	private ProductService pro;
	
	@GetMapping("/get/product_shopname")
	public JSONObject getByProduct (@RequestParam("shopName") String shopName, @RequestParam("page") int page, @RequestParam("size") int size) {
		return pro.getByProduct(shopName, page, size);
	}
	 
	

}
