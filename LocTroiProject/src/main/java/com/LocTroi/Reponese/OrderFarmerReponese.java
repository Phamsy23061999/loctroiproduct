package com.LocTroi.Reponese;

import java.util.Date;
import java.util.List;

import com.LocTroi.DTO.citiesDTO;
import com.LocTroi.DTO.districtsDTO;
import com.LocTroi.DTO.farmerDTO;
import com.LocTroi.DTO.orderFarmerDTO;
import com.LocTroi.DTO.userDTO;
import com.LocTroi.DTO.wardsDTO;
import com.LocTroi.Model.citiesEntity;
import com.LocTroi.Model.districtsEntity;
import com.LocTroi.Model.farmerEntity;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Model.userEntity;
import com.LocTroi.Model.wardsEntity;


public class OrderFarmerReponese {
	
	private int id;
	private String orderCode = "";
	private int status;
	private Date createDate;
	
	private String farmerName = "";
	private String firstName = "";
	private String shopName = "";
	private String cityName = "";
	private String districtsName = "";
	private String wardName = "";
	
	public OrderFarmerReponese() {
		
	}
	
	
	
	
	public OrderFarmerReponese(orderFarmerEntity order,farmerEntity farmer,userEntity user,citiesEntity city,districtsEntity ditricts, wardsEntity wards) {
		
		this.id = order.getId();
		this.orderCode= order.getOrderCode();
		this.status = order.getStatus();
		this.createDate = order.getCreateDate();
		
		if(farmer != null) {
			this.farmerName = farmer.getFarmerName();
		}
		
		if (user != null) {
			this.firstName = user.getFirstName();
			this.shopName = user.getShopName();
		}
		if(city != null) {
			this.cityName = city.getName();
		}
		if(ditricts != null) {
			this.districtsName = ditricts.getName();
		}
		if(wards != null) {
			this.wardName = wards.getName();
		}
		
		
	}




	public String getWardName() {
		return wardName;
	}
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}
	public String getDistrictsName() {
		return districtsName;
	}
	public void setDistrictsName(String districtsName) {
		this.districtsName = districtsName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getFarmerName() {
		return farmerName;
	}
	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
		
	
}
