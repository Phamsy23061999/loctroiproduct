package com.LocTroi.Reponese;

import com.LocTroi.Model.ProductEntity;

public class ProductReponse {
	
	private int id;
	private String productCode = "";
	private String name = "";
	private Double price;
	private int minUnit;
	private int maxUnit;
	private String attribute = "";
	private String imageThumbnail = "";
	
	public ProductReponse() {
		
	}
	
	public ProductReponse(ProductEntity pro) {
		this.id = pro.getId();
		this.name = pro.getName();
		this.price= pro.getPrice();
		this.productCode = pro.getProductCode();
		this.imageThumbnail=pro.getImageThumbnail();
		this.maxUnit=pro.getMaxUnit();
		this.minUnit= pro.getMinUnit();
		this.attribute = pro.getAttribute();
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getMinUnit() {
		return minUnit;
	}
	public void setMinUnit(int minUnit) {
		this.minUnit = minUnit;
	}
	public int getMaxUnit() {
		return maxUnit;
	}
	public void setMaxUnit(int maxUnit) {
		this.maxUnit = maxUnit;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getImageThumbnail() {
		return imageThumbnail;
	}
	public void setImageThumbnail(String imageThumbnail) {
		this.imageThumbnail = imageThumbnail;
	}
	

}
