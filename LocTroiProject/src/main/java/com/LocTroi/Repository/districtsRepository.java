package com.LocTroi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.districtsDTO;
import com.LocTroi.Model.districtsEntity;
import com.LocTroi.Model.orderFarmerEntity;
@Repository
public interface districtsRepository extends  JpaRepository<districtsEntity, Integer> {
	
	@Query(value = "SELECT di.* FROM districts di WHERE di.id = ?1 ", nativeQuery = true)
	districtsEntity findById(int districtsId);

}
