package com.LocTroi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.wardsDTO;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Model.wardsEntity;

@Repository
public interface wardsRepository extends JpaRepository<wardsEntity, Integer>{
	
	
	@Query(value = "SELECT wa.* From wards wa where wa.id = ?1  ",nativeQuery = true)
	 wardsEntity findById(int wardId);
}
