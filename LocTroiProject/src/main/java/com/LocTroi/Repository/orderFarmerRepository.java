package com.LocTroi.Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



import com.LocTroi.Model.orderFarmerEntity;

import net.minidev.json.JSONObject;





@Repository
public interface orderFarmerRepository extends JpaRepository<orderFarmerEntity, Integer> {
	
	@Query(value="SELECT od.* FROM order_farmer od  WHERE Date(od.create_date) >= date(?1) and date(od.create_date) <= date(?2)",nativeQuery = true)
	List<orderFarmerEntity> findByFarmerOrderCreatedate(@Param("dateStart") Date dateStart, @Param("dateFinish") Date dateFinish);
	
	
	@Query(value = "SELECT od.* FROM order_farmer od, user_agency_detail uad , agency ag, agency_user_agency_detail auad Where od.user_id = uad.user_id AND uad.user_agency_id=auad.user_agency_id and  auad.agency_id = ag.id and ag.name = ?1 ", nativeQuery = true)
	List<orderFarmerEntity> finByOrderAgancy(@Param("agencyName") String agencyName);
	
	
	@Query(value = "SELECT od.* FROM order_farmer od, wards wa WHERE od.ward_id = wa.id AND wa.name = ?1", nativeQuery = true)
	List<orderFarmerEntity> finByOrderWard(@Param("wardName") String wardName);
	
	@Query(value = "SELECT od.* FROM order_farmer od, districts di WHERE od.district_id = di.id AND di.name = ?1", nativeQuery = true)
	List<orderFarmerEntity> finByOrderDistrict(@Param("districtsName") String districtsName);
	
	
	@Query(value = "SELECT od.* FROM order_farmer od, cities ci WHERE od.city_id = ci.id AND ci.name =?1 LIMIT 1", nativeQuery = true)
	List<orderFarmerEntity> finByOrderCiti(@Param("citiName") String citiName);
	
	
	@Query(value= "SELECT od.*,us.shop_name,ag.name FROM order_farmer od LEFT JOIN  user us on od.user_id = us.id "
			+ " LEFT JOIN user_agency_detail uad on od.user_id = uad.user_id"
			+ " LEFT JOIN agency_user_agency_detail auad on uad.user_agency_id=auad.user_agency_id"
			+ " LEFT JOIN agency ag on  auad.agency_id = ag.id "
			+ " LEFT JOIN sale_man sm on ag.id = sm.agency_id"
			+ " LEFT JOIN wards wa on od.ward_id = wa.id"
			+ " LEFT JOIN districts di on od.district_id = di.id"
			+ " LEFT JOIN cities ci on od.city_id = ci.id"
			+ " WHERE Date(od.create_date) >= date(?1) and date(od.create_date) <= date(?2)"
			+ " AND ( od.city_id = 0 or od.city_id = ?3 ) "
			+ " AND ( od.ward_id = 0 or od.ward_id = ?4 ) "
			+ " AND ( od.district_id = 0 or od.district_id = ?5 ) "
			+ " AND ( ag.id =0 or ag.id = ?6 ) "
			+ " LIMIT 1", nativeQuery = true)
	List<orderFarmerEntity> findByOrderAll(@Param("dateStart") Date dateStart, 
			@Param("dateFinish") Date dateFinish, @Param("cityId") int cityId,
			@Param("wardId") int wardId,@Param("districtId") int districtId,
			@Param("agencyId") int agencyId );
	
	
	
	@Query(value = "SELECT od.*,us.shop_name,ag.name FROM order_farmer od LEFT JOIN  user us on od.user_id = us.id"
			+ " LEFT JOIN user_agency_detail uad on od.user_id = uad.user_id "
			+ " LEFT JOIN agency_user_agency_detail auad on uad.user_agency_id=auad.user_agency_id"
			+ " LEFT JOIN agency ag on  auad.agency_id = ag.id "
			+ " LEFT JOIN sale_man sm on ag.id = sm.agency_id "
			+ " LEFT JOIN wards wa on od.ward_id = wa.id "
			+ " LEFT JOIN districts di on od.district_id = di.id"
			+ " LEFT JOIN cities ci on od.city_id = ci.id "
			+ " WHERE(od.order_code like %:keysearch%) or (od.fullname like %:keysearch%)"
			+ "	LIMIT :begin, :size", nativeQuery = true)
	List<orderFarmerEntity> getByOrderKeyName(@Param("keysearch") String keysearch,@Param("begin") int begin ,@Param("size") int size);
	
	@Query (value = " select count(*) from (SELECT od.*,us.shop_name,ag.name " + 
			" FROM order_farmer od " + 
			" LEFT JOIN user us on od.user_id = us.id " + 
			" LEFT JOIN user_agency_detail uad on od.user_id = uad.user_id " + 
			" LEFT JOIN agency_user_agency_detail auad on uad.user_agency_id=auad.user_agency_id " + 
			" LEFT JOIN agency ag on auad.agency_id = ag.id LEFT JOIN sale_man sm on ag.id = sm.agency_id " + 
			" LEFT JOIN wards wa on od.ward_id = wa.id " + 
			" LEFT JOIN districts di on od.district_id = di.id " + 
			" LEFT JOIN cities ci on od.city_id = ci.id " + 
			" WHERE(od.order_code like %:keysearch%) or (od.fullname like %:keysearch%))as taoalo",nativeQuery = true)
	int totalOrder(@Param("keysearch") String keysearch);
}




