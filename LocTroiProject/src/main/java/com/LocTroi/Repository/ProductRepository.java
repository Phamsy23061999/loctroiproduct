package com.LocTroi.Repository;

import java.util.List;

import org.aspectj.weaver.tools.Trace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.LocTroi.Model.ProductEntity;

@Repository
public interface ProductRepository  extends JpaRepository<ProductEntity,Integer> {
	
	@Query(value = "SELECT pr.* FROM product pr "
			+ " LEFT JOIN order_child_product  ocp ON pr.id = ocp.product_id "
			+ " LEFT JOIN order_child odc ON  odc.order_code_child = ocp.order_child"
			+ "	where odc.shop_name = ?1"
			+ "	LIMIT ?2, ?3", nativeQuery = true)
	List<ProductEntity> getByProduct(@Param("shopName") String shopName,@Param("begin") int begin,@Param("size") int size);
	
	@Query (value = "select count(*) from (" + 
			" SELECT pr.*" + 
			" FROM product pr" + 
			" LEFT JOIN order_child_product  ocp ON pr.id = ocp.product_id " + 
			" LEFT JOIN order_child odc ON  odc.order_code_child = ocp.order_child " + 
			" WHERE odc.shop_name = ?1  ) as taolao ",nativeQuery = true)
	int totalOrder ( @Param("shopName") String shopName);
			
}
