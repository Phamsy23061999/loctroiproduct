package com.LocTroi.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.citiesDTO;
import com.LocTroi.Model.citiesEntity;
import com.LocTroi.Model.orderFarmerEntity;

@Repository
public interface cityRepository extends JpaRepository<citiesEntity, Integer>{
	
	@Query(value = "SELECT ci.* FROM cities ci where ci.id = ?1 ",nativeQuery = true)
	citiesEntity findByCity(int citiId);

}

 