package com.LocTroi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.farmerDTO;
import com.LocTroi.Model.farmerEntity;
import com.LocTroi.Model.orderFarmerEntity;

@Repository
public interface farmerRepository extends JpaRepository<farmerEntity, Integer>{
	
	@Query(value = "SELECT fm.* FROM farmer fm where fm.id = ?1",nativeQuery = true)
	 farmerEntity findById(int farmerId);
}
