package com.LocTroi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.LocTroi.DTO.userDTO;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Model.userEntity;

@Repository
public interface userRepository extends JpaRepository<userEntity, Integer> {
	@Query(value = "SELECT us.* FROM user us where us.id = ?1 ",nativeQuery = true)
	userEntity findById(int userId);
	
	
}
