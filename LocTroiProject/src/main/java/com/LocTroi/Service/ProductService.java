package com.LocTroi.Service;

import org.springframework.stereotype.Service;

import net.minidev.json.JSONObject;

@Service
public interface ProductService {
	
	JSONObject getByProduct(String shopName, int page, int size);
}
