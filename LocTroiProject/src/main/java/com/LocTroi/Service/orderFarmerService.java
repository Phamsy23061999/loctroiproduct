package com.LocTroi.Service;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.LocTroi.Model.orderFarmerEntity;

import net.minidev.json.JSONObject;

@Service
public interface orderFarmerService {
	
	JSONObject findByOrderFarmer(Date dateStart, Date dateFinish);
	
	JSONObject findByOrderAgency ( String agencyName);
	
	JSONObject findByOrderDistricts ( String districtsName);
	JSONObject findByOrderWard ( String wardName);
	JSONObject findByOrderCiti ( String citiName);
	JSONObject finByOrderAll(Date dateStart, Date dateFinish, int cityId, int wardId, int districtId, int agencyId);
	JSONObject findByOrder( String keysearch, int page, int size);
	JSONObject getByProduct(String shopName, int page, int size);

}
