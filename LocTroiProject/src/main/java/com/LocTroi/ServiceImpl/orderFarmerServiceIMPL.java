package com.LocTroi.ServiceImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.LocTroi.DAO.orderFarmerDAO;
import com.LocTroi.Model.ProductEntity;
import com.LocTroi.Model.orderFarmerEntity;
import com.LocTroi.Reponese.ProductReponse;
import com.LocTroi.Repository.ProductRepository;
import com.LocTroi.Service.orderFarmerService;

import net.minidev.json.JSONObject;

@Service
public class orderFarmerServiceIMPL implements orderFarmerService{

	@Autowired 
	orderFarmerDAO orderDAO; 
	
	@Override
	public JSONObject findByOrderFarmer(Date dateStart, Date dateFinish) {
		JSONObject js = new JSONObject();
		
		try {
			
		js.put("List ", orderDAO.getByOrderFarmer(dateStart, dateFinish));
		}catch (Exception e) {
			js.put("Error", "Có Lỗi Xảy Ra");
		}
		return js;
	}
	
	@Override
	public JSONObject findByOrderAgency(String agencyName) {
		JSONObject js = new JSONObject();
		try {
			js.put("List Order Agency", orderDAO.finByOrderAgency(agencyName));
		} catch (Exception e) {
			js.put("Error", "Có Lỗi Xảy Ra");
		}
		return js;
		
	}
	@Override
	public JSONObject findByOrderDistricts(String districtsName) {
		JSONObject js = new JSONObject();
		try {
			js.put("List Order Agency", orderDAO.finByOrderDistrict(districtsName));
		} catch (Exception e) {
			js.put("Error", "Có Lỗi Xảy Ra");
		}
		return js;
	}
	@Override
	public JSONObject findByOrderWard(String wardName) {
		JSONObject js = new JSONObject();
		try {
			js.put("List Order Agency", orderDAO.finByOrderWard(wardName));
		} catch (Exception e) {
			js.put("Error", "Có Lỗi Xảy Ra");
		}
		return js;
	}
	@Override
	public JSONObject findByOrderCiti(String citiName) {
		JSONObject js = new JSONObject();
		try {
			js.put("List Order Agency", orderDAO.finByOrderCiti(citiName));
		} catch (Exception e) {
			js.put("Error", "Có Lỗi Xảy Ra");
		}
		return js;
	}

	@Override
	public JSONObject finByOrderAll(Date dateStart, Date dateFinish, int cityId, int wardId, int districtId,int agencyId) {
		JSONObject js = new JSONObject();
		try {
			js.put("List Order", orderDAO.findByOrderAll(dateStart, dateFinish, cityId, wardId, districtId, agencyId));
		} catch (Exception e) {
			js.put("Error", "Có lỗi Xảy ra");
		}
		return js;
	}

	@Override
	public JSONObject findByOrder(String keysearch, int page, int size) {
		JSONObject js = new JSONObject();
		try {
			js.put("List",orderDAO.getByOrderKeyName(keysearch,page,size));
		} catch (Exception e) {
			js.put("Error", "Có lỗi Xảy ra");
		}
		return js;
	}

	@Override
	public JSONObject getByProduct(String shopName, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}


}
