package com.LocTroi.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.LocTroi.Model.ProductEntity;
import com.LocTroi.Reponese.ProductReponse;
import com.LocTroi.Repository.ProductRepository;
import com.LocTroi.Service.ProductService;

import net.minidev.json.JSONObject;

@Service
public class ProductServiceIMPL implements ProductService{
	
	@Autowired
	ProductRepository repository;
	
	@Override
	public JSONObject getByProduct(String shopName, int page, int size) {
		JSONObject js = new JSONObject();
		try {
			int begin = (page - 1)* size ;
			
			
			List<ProductEntity> product = repository.getByProduct(shopName, begin, size);
			int total = repository.totalOrder(shopName);
			List<ProductReponse> ListRepone = new ArrayList<ProductReponse>();
			ProductReponse res = new ProductReponse();
		
			
			for(ProductEntity pro : product) {
				
				ProductReponse repone = new ProductReponse(pro); 
				
				
				
//				ProductReponse repone = new ProductReponse();
//				repone.setId(pro.getId());
//				repone.setAttribute(pro.getAttribute());
//				repone.setImageThumbnail(pro.getImageThumbnail());
//				repone.setName(pro.getName());
//				repone.setPrice(pro.getPrice());
//				repone.setProductCode(pro.getProductCode());
//				repone.setMaxUnit(pro.getMaxUnit());
//				repone.setMinUnit(pro.getMinUnit());
				
				
				
				ListRepone.add(repone);
				js.put("List Product", ListRepone);
			}
			
			js.put("Size", size);
			js.put("total", total);
			
			
		} catch (Exception e) {
			js.put("Error","Có Lỗi Xảy Ra");
		}
		return js;
	}

}
